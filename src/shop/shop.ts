import { ConstructionData, ConstructionsData } from "../data/constructions";
import { ItemData, ItemsData } from "../data/items";

export class Shop extends Phaser.GameObjects.GameObject {
  private constructionsContainer: Phaser.GameObjects.Container;
  private itemsContainer: Phaser.GameObjects.Container;

  constructor(scene: Phaser.Scene) {
    super(scene, "shop");

    this.scene.add.rectangle(400, 250, 600, 480, 0x125555);
    this.createConstructions();
    this.createItems();
  }

  private selectConstruction(constructionData: ConstructionData) {
    this.scene.game.registry.set("currentConstruction", constructionData);
    this.scene.game.events.emit("currentConstructionChanged", constructionData);
  }

  private sellItem(itemData: ItemData) {
    const itemId = itemData.id;
    const inventory = this.scene.game.registry.get("inventory");
    const inventoryAmount = inventory.get(itemId);
    const revenue = inventoryAmount * itemData.cost;
    if (revenue > 0) {
      this.scene.game.registry.inc("money", revenue);
      this.scene.game.events.emit(
        "moneyChanged",
        this.scene.game.registry.get("money")
      );
      inventory.set(itemId, 0);
      this.scene.game.events.emit("inventoryChanged");
    }
  }

  //TODO: use autolayout
  private createConstructions() {
    const itemWidth = 150;
    this.constructionsContainer = this.scene.add.container(230, 90);

    let index = 0;
    for (let id in ConstructionsData) {
      const constructionData = ConstructionsData[id];
      const x = index * (itemWidth + 20);
      const y = 50;

      //TODO: move to shopItem
      const background = new Phaser.GameObjects.Rectangle(
        this.scene,
        x,
        y,
        itemWidth,
        200,
        0x197c7c
      );
      background
        .setInteractive({ useHandCursor: true })
        .on("pointerup", () => this.selectConstruction(constructionData));
      this.constructionsContainer.add(background);

      const sprite = new Phaser.GameObjects.Sprite(
        this.scene,
        x,
        y - 40,
        constructionData.image
      );
      this.constructionsContainer.add(sprite);

      const info = `
      ${constructionData.name}

      cost: ${constructionData.cost} $
      consume: ${
        constructionData.consume
          ? constructionData.consume.amount +
            " " +
            constructionData.consume.item.name
          : "-"
      }
      produce: ${constructionData.produce.amount} ${
        constructionData.produce.item.name
      }
      time: ${constructionData.productionTime} sec
      `;
      const text = new Phaser.GameObjects.Text(
        this.scene,
        x - 100,
        y - 10,
        info,
        {
          fontSize: "12px",
        }
      );
      this.constructionsContainer.add(text);

      index++;
    }
  }

  //TODO: remove duplication
  private createItems() {
    const itemWidth = 150;
    this.itemsContainer = this.scene.add.container(230, 320);
    let index = 0;
    for (let id in ItemsData) {
      const itemData = ItemsData[id];
      const x = index * (itemWidth + 20);
      const y = 50;

      //TODO: move to shopItem
      const background = new Phaser.GameObjects.Rectangle(
        this.scene,
        x,
        y,
        itemWidth,
        200,
        0x197c7c
      );
      this.itemsContainer.add(background);

      const sprite = new Phaser.GameObjects.Sprite(
        this.scene,
        x,
        y - 40,
        itemData.image
      );
      this.itemsContainer.add(sprite);

      const info = `
      ${itemData.name}

      cost: ${itemData.cost} $`;
      const costText = new Phaser.GameObjects.Text(
        this.scene,
        x - 100,
        y - 10,
        info,
        {
          fontSize: "12px",
        }
      );
      this.itemsContainer.add(costText);

      const amountText = new Phaser.GameObjects.Text(
        this.scene,
        x - 57,
        y + 45,
        `amount: ${this.scene.game.registry.get("inventory").get(itemData.id)}`,
        {
          fontSize: "12px",
        }
      );
      this.itemsContainer.add(amountText);

      this.itemsContainer.add(
        new Phaser.GameObjects.Text(this.scene, x - 20, y + 70, "SELL", {
          fontSize: "12px",
        })
      );

      background.setInteractive({ useHandCursor: true }).on("pointerup", () => {
        this.sellItem(itemData);
        amountText.setText("amount: 0");
      });

      index++;
    }
  }
}
