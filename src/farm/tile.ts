import { ConstructionData } from "../data/constructions";
import { Construction } from "./construction/construction";

export class Tile extends Phaser.GameObjects.GameObject {
  readonly x: number;
  readonly y: number;
  private sprite: Phaser.GameObjects.Sprite;
  private registry: Phaser.Data.DataManager;
  private construction: Construction | null;

  constructor(scene: Phaser.Scene, x: number, y: number) {
    super(scene, "tile");

    this.x = x;
    this.y = y;
    this.registry = scene.game.registry;
    this.construction = null;
    this.sprite = scene.add.sprite(x, y, "ground");
    this.sprite.setInteractive({ useHandCursor: true });
    this.sprite.on("pointerdown", () => this.handleClick());
    this.sprite.on("pointerover", () => scene.events.emit("hover", this));
  }

  update(time: number, delta: number) {
    if (!this.construction) return;

    this.construction.update(time, delta);
  }

  private handleClick() {
    if (this.construction) {
      this.construction.handleClick();
    } else {
      const currentConstruction = this.registry.get("currentConstruction");
      if (currentConstruction) {
        this.build(currentConstruction);
      }
    }
  }

  private build(constructionData: ConstructionData) {
    if (this.construction) return;

    const money = this.registry.get("money");
    if (money < constructionData.cost) return;

    this.registry.set("money", money - constructionData.cost);
    this.scene.game.events.emit(
      "moneyChanged",
      this.scene.game.registry.get("money")
    );
    this.construction = new Construction(
      this.scene,
      this.x,
      this.y,
      constructionData,
      this.registry
    );

    this.scene.game.events.emit("tileBuiltUp", this);
  }
}
