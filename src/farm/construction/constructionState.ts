import { ItemPackData } from "../../data/items";
import { Construction } from "./construction";

export abstract class ConstructionState {
  protected construction: Construction;

  constructor(construction: Construction) {
    this.construction = construction;
  }

  handleClick(): void {}

  update(time: number, delta: number): void {}
}

export class Consume extends ConstructionState {
  private itemPackData: ItemPackData;

  constructor(construction: Construction) {
    super(construction);

    const constructionData = construction.getConstructionData();
    this.itemPackData = constructionData.consume;
    if (this.itemPackData) {
      this.construction.showConsumePopup();
    }
  }

  handleClick(): void {
    if (this.itemPackData) {
      const registry = this.construction.getRegistry();
      const inventory = registry.get("inventory");
      const requiredAmount = this.itemPackData.amount;
      const requiredId = this.itemPackData.item.id;
      const inventoryAmount = inventory.get(requiredId);
      if (inventoryAmount >= requiredAmount) {
        inventory.set(requiredId, inventoryAmount - requiredAmount);
        this.construction.hideConsumePopup();
        this.construction.setConstructionState(new Produce(this.construction));
      }
    }
  }

  update(time: number, delta: number): void {
    if (!this.itemPackData) {
      this.construction.setConstructionState(new Produce(this.construction));
    }
  }
}

export class Produce extends ConstructionState {
  private productionProgress: number = 0;
  private productionTime: number = 0;

  constructor(construction: Construction) {
    super(construction);

    const constructionData = construction.getConstructionData();
    this.productionTime = constructionData.productionTime;
  }

  update(time: number, delta: number) {
    this.productionProgress += delta / 1000;
    this.construction.setProgress(
      this.productionProgress / this.productionTime
    );
    if (this.productionProgress >= this.productionTime) {
      this.construction.setConstructionState(new Ready(this.construction));
    }
  }
}

export class Ready extends ConstructionState {
  private itemPackData: ItemPackData;

  constructor(construction: Construction) {
    super(construction);

    const constructionData = construction.getConstructionData();
    this.itemPackData = constructionData.produce;
  }

  handleClick() {
    const registry = this.construction.getRegistry();
    const inventory = registry.get("inventory");
    const productionAmount = this.itemPackData.amount;
    const productionId = this.itemPackData.item.id;
    const inventoryAmount = inventory.get(productionId);
    inventory.set(productionId, inventoryAmount + productionAmount);
    this.construction.setConstructionState(new Produce(this.construction));
  }
}
