import { ConstructionData } from "../../data/constructions";
import { ConstructionState, Consume } from "./constructionState";

export class Construction extends Phaser.GameObjects.GameObject {
  private sprite: Phaser.GameObjects.Sprite;
  private popup: Phaser.GameObjects.Sprite;
  private progressBar: Phaser.GameObjects.Rectangle;
  private constructionData: ConstructionData;
  private constructionState: ConstructionState;
  private registry: Phaser.Data.DataManager;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    constructionData: ConstructionData,
    registry: Phaser.Data.DataManager
  ) {
    super(scene, "construction");

    this.constructionData = constructionData;
    this.registry = registry;
    this.sprite = scene.add.sprite(x, y, constructionData.image);
    if (constructionData.consume) {
      this.popup = this.scene.add
        .sprite(x + 20, y - 20, constructionData.consume.item.image)
        .setScale(0.5, 0.5)
        .setAlpha(0);
    }

    this.progressBar = scene.add.rectangle(x - 25, y + 22, 0, 6, 0xf44336);
    this.setConstructionState(new Consume(this));
  }

  handleClick() {
    this.constructionState.handleClick();
  }

  update(time: number, delta: number) {
    this.constructionState.update(time, delta);
  }

  setConstructionState(state: ConstructionState) {
    this.constructionState = state;
  }

  setProgress(progress: number) {
    this.progressBar.width = progress * 50;
  }

  showConsumePopup() {
    if (this.popup) {
      this.popup.setAlpha(1);
    }
  }

  hideConsumePopup() {
    if (this.popup) {
      this.popup.setAlpha(0);
    }
  }

  getConstructionData(): ConstructionData {
    return this.constructionData;
  }

  getRegistry(): Phaser.Data.DataManager {
    return this.registry;
  }
}
