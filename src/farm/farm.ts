import { Tile } from "./tile";

export class Farm extends Phaser.GameObjects.GameObject {
  static readonly width: number = 8;
  static readonly height: number = 8;
  static readonly tileSize: number = 50;

  tiles: Phaser.Structs.Set<Tile>;
  tilesWithConstruction: Phaser.Structs.Set<Tile>;
  hover: Phaser.GameObjects.Rectangle;

  constructor(scene: Phaser.Scene) {
    super(scene, "farm");

    this.tiles = new Phaser.Structs.Set<Tile>();
    this.tilesWithConstruction = new Phaser.Structs.Set<Tile>();
    this.createTiles(scene);

    this.hover = scene.add.rectangle(0, 0, 50, 50, 0x00ddbb).setAlpha(0);
    scene.events.on("hover", (tile: Tile) => {
      this.hover.alpha = 0.2;
      this.hover.setPosition(tile.x, tile.y);
    });
    scene.game.events.on("clearHover", () => (this.hover.alpha = 0));
    scene.game.events.on("tileBuiltUp", (tile: Tile) =>
      this.tilesWithConstruction.set(tile)
    );
  }

  update(time: number, delta: number) {
    this.tilesWithConstruction.entries.forEach((tile) =>
      tile.update(time, delta)
    );
  }

  private createTiles(scene: Phaser.Scene) {
    const tilesOffsetX =
      scene.cameras.main.centerX -
      (Farm.tileSize * Farm.width) / 2 +
      Farm.tileSize / 2;
    const tilesOffsetY = 50;

    for (let x = 0; x < Farm.width; x++) {
      for (let y = 0; y < Farm.height; y++) {
        const offsetX = x * Farm.tileSize + tilesOffsetX;
        const offsetY = y * Farm.tileSize + tilesOffsetY;

        const tile = new Tile(scene, offsetX, offsetY);
        this.tiles.set(tile);
      }
    }
  }
}
