export class TextButton extends Phaser.GameObjects.Text {
  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    text: string,
    callback: () => void
  ) {
    super(scene, x, y, text, { backgroundColor: "#125555", fontSize: "20px" });

    this.setInteractive({ useHandCursor: true });
    this.on("pointerover", () => this.setOverState());
    this.on("pointerout", () => this.setOutState());
    this.on("pointerdown", () => this.setDownState());
    this.on("pointerup", () => {
      this.setOverState();
      callback();
    });

    this.scene.add.existing(this);
  }

  private setOverState() {
    this.setStyle({ fill: "#f5df4d" });
  }

  private setOutState() {
    this.setStyle({ fill: "#ffffff" });
  }

  private setDownState() {
    this.setStyle({ fill: "#ffffff" });
  }
}
