import "phaser";
import { LoadScene, FarmScene, HUDScene, ShopScene } from "./scenes";

const config = {
  type: Phaser.AUTO,
  backgroundColor: "#125555",
  width: 800,
  height: 600,
  parent: "game",
  scale: {
    mode: Phaser.Scale.FIT,
  },
  scene: [LoadScene, FarmScene, HUDScene, ShopScene],
};

export class FarmGame extends Phaser.Game {
  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);

    this.registry.set("currentConstruction", null);
    //TODO: move to inventory component
    this.registry.set(
      "inventory",
      new Phaser.Structs.Map([
        ["wheat", 0],
        ["egg", 0],
        ["milk", 0],
      ])
    );
    this.registry.set("money", 1);
  }
}

window.onload = () => {
  const game = new FarmGame(config);
};
