import { Shop } from "../shop/shop";

export default class ShopScene extends Phaser.Scene {
  constructor() {
    super("shopScene");
  }

  create() {
    new Shop(this);
  }
}
