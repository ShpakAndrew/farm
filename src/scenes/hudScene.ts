import { ConstructionData } from "../data/constructions";
import { TextButton } from "../ui/textButton";

export default class HUDScene extends Phaser.Scene {
  private shopButton: TextButton;
  private isShopOpened: boolean = false;
  private currentConstruction: Phaser.GameObjects.Sprite;
  private moneyText: Phaser.GameObjects.Text;

  constructor() {
    super("hudScene");
  }

  create() {
    const width = Number.parseInt(this.game.config.width.toString());
    const height = Number.parseInt(this.game.config.height.toString());

    this.add
      .rectangle(0, height - 100, width, 100, 0x197c7c)
      .setOrigin(0, 0)
      .setInteractive({ cursor: "default" })
      .on("pointerover", () => this.game.events.emit("clearHover"));

    this.shopButton = new TextButton(this, width / 2, height - 50, "SHOP", () =>
      this.toggleShop()
    ).setOrigin(0.5, 0.5);

    const money = this.game.registry.get("money");
    this.moneyText = this.add.text(width - 80, height - 60, `${money}$`, {
      fontSize: "24px",
    });

    this.game.events.on(
      "currentConstructionChanged",
      (constructionData: ConstructionData) => {
        this.toggleShop();
        if (this.currentConstruction) {
          this.currentConstruction.setTexture(constructionData.image);
        } else {
          this.currentConstruction = this.add.sprite(
            50,
            height - 50,
            constructionData.image
          );
        }
      }
    );

    this.game.events.on("moneyChanged", (money: number) => {
      this.moneyText.setText(`${money}$`);
    });
  }

  private toggleShop() {
    if (this.isShopOpened) {
      this.scene.stop("shopScene");
    } else {
      this.scene.run("shopScene");
    }

    this.isShopOpened = !this.isShopOpened;
    this.shopButton.setText(this.isShopOpened ? "BACK" : "SHOP");
  }
}
