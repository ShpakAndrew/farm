import { Farm } from "../farm/farm";

export default class LoadScene extends Phaser.Scene {
  private farm: Farm;

  constructor() {
    super("farmScene");
  }

  create() {
    this.farm = new Farm(this);
  }

  update(time: number, delta: number) {
    this.farm.update(time, delta);
  }
}
