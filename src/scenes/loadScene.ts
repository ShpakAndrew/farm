export default class LoadScene extends Phaser.Scene {
  constructor() {
    super("loadScene");
  }

  preload() {
    this.add
      .text(
        this.cameras.main.centerX,
        this.cameras.main.centerY,
        "Loading...",
        { fontSize: "24px" }
      )
      .setAlign("center")
      .setOrigin(0.5, 0.5);

    this.loadAssets();
    this.load.on("complete", () => {
      this.scene.start("farmScene");
      this.scene.start("hudScene");
    });
  }

  private loadAssets() {
    //TODO: use atlas
    this.load.image("ground", "assets/images/ground.png");
    this.load.image("wheatField", "assets/images/wheatField.png");
    this.load.image("wheat", "assets/images/wheat.png");
    this.load.image("henhouse", "assets/images/henhouse.png");
    this.load.image("egg", "assets/images/egg.png");
    this.load.image("cowshed", "assets/images/cowshed.png");
    this.load.image("milk", "assets/images/milk.png");
  }
}
