export interface ItemPackData {
  item: ItemData;
  amount: number;
}

export interface ItemData {
  id: string;
  name: string;
  image: string;
  cost: number;
}

export const ItemsData: { [key: string]: ItemData } = {
  wheat: {
    id: "wheat",
    name: "Wheat",
    image: "wheat",
    cost: 1,
  },
  egg: {
    id: "egg",
    name: "Egg",
    image: "egg",
    cost: 5,
  },
  milk: {
    id: "milk",
    name: "Milk",
    image: "milk",
    cost: 10,
  },
};
