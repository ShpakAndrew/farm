import { ItemPackData, ItemsData } from "./items";

export interface ConstructionData {
  name: string;
  image: string;
  consume: ItemPackData | null;
  produce: ItemPackData;
  productionTime: number;
  cost: number;
}

export const ConstructionsData: { [key: string]: ConstructionData } = {
  field: {
    name: "Wheat Field",
    image: "wheatField",
    consume: null,
    produce: {
      item: ItemsData["wheat"],
      amount: 1,
    },
    productionTime: 10,
    cost: 1,
  },
  henhouse: {
    name: "Henhouse",
    image: "henhouse",
    consume: {
      item: ItemsData["wheat"],
      amount: 1,
    },
    produce: {
      item: ItemsData["egg"],
      amount: 3,
    },
    productionTime: 30,
    cost: 5,
  },
  cowshed: {
    name: "Cowshed",
    image: "cowshed",
    consume: {
      item: ItemsData["wheat"],
      amount: 1,
    },
    produce: {
      item: ItemsData["milk"],
      amount: 1,
    },
    productionTime: 10,
    cost: 10,
  },
};
