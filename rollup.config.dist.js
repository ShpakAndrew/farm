import resolve from "rollup-plugin-node-resolve";
import commonjs from '@rollup/plugin-commonjs';
import typescript from "rollup-plugin-typescript2";

export default {
  input: [ "./src/app.ts" ],
  output: {
    file: "./dist/app.js",
    name: "farm",
    format: "iife",
    sourcemap: false
  },
  plugins: [
    resolve({
      extensions: [ ".ts", ".tsx" ]
    }),
    commonjs({
      include: [
        "node_modules/eventemitter3/**",
        "node_modules/phaser/**"
      ],
      exclude: [ 
        "node_modules/phaser/src/polyfills/requestAnimationFrame.js"
      ],
      sourceMap: true
    }),
    typescript()
  ]
};